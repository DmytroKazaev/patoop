package org;

/**
 * Created by dmytro on 15.06.16.
 */
public abstract class Animal {
    private String name;
    private int cheerfulness = 100;

    public Animal(String name) {
        this.name = name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public String getName() {
        return name;
    }

    public void setCheerfulness(int cheerfulness) {
        this.cheerfulness = cheerfulness;
    }
    public int getCheerfulness() {
        return cheerfulness;
    }

    public abstract void voice();

    public void sit(){
        System.out.println("I'm sit.");
        cheerfulness -= 5;
    }

    public void jump(){
        System.out.println("I have jumped.");
        cheerfulness -= 10;
    }

    public void eat(){
        System.out.println("I have eaten.");
        cheerfulness += 20;
    }

    public void sleep(){
        System.out.println("I have slept.");
        cheerfulness += 50;
    }
}
