package org;

/**
 * Created by dmytro on 15.06.16.
 */
public class Dog extends Animal {

    public Dog(String name) {
        super(name);

    }

    @Override
    public void voice() {
        System.out.println("Gav, gav!");

    }
}
