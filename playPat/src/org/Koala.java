package org;

/**
 * Created by dmytro on 15.06.16.
 */
public class Koala extends Animal {

    public Koala(String name) {
        super(name);
    }

    @Override
    public void voice() {
        System.out.println("Khrrrr!");

    }


}
