package org;

import java.util.Scanner;

/**
 * Created by dmytro on 15.06.16.
 */
public class Performer {

    private static Scanner scanner = new Scanner(System.in);
    private static Animal animal;



    public  static  void start() {
        System.out.println("This is Tamagotchi!\nChoose the number of pat:\n" +
                "1. Koala" + "\n2. Cat" + "\n3. Dog");
        int number = scanner.nextInt();
        String nameOfPat = scanner.nextLine();


        switch (number) {
            case 1:
                System.out.println("Give the name for the koala");
                animal = new Koala(scanner.nextLine());
                break;
            case 2:
                System.out.println("Give the name for the cat");
                animal = new Cat(scanner.nextLine());
                break;
            case 3:
                System.out.println("Give the name for the dog");
                animal = new Dog(scanner.nextLine());
                break;
            default:
                System.out.println("We don't have the pat with this number!\n Try again.");
                start();
        }

        System.out.println("You choose a " + animal.getName());
        System.out.println("Play with him, using these keys: \n1. Say. \n2. Sit. \n3. Jump. \n4. Eat. \n5. Sleep.\n 0. Exit");

        do {
            int doing = scanner.nextInt();
            switch (doing) {
                case 0:
                    break;
                case 1:
                    animal.voice();
                    break;
                case 2:
                    animal.sit();
                    break;
                case 3:
                    animal.jump();
                    break;
                case 4:
                    animal.eat();
                    break;
                case 5:
                    animal.sleep();
                    break;
                default:
                    System.out.println("Pat doesn't now what to do. Input one more time.");
                    continue;
            }
            if (doing == 0){
                System.out.println("It was very funny. See later.");
                break;
            }

            if (animal.getCheerfulness() <= 0 ) {
                System.out.println("Your pat is very tired. \n" +
                        "The next time you watch the indicator of vitality.");
                break;
            } else if (animal.getCheerfulness() > 100) {
                System.out.println("I am full of energy.");
                animal.setCheerfulness(100);
            }

            System.out.println("Cheerfulness of your pat is " + animal.getCheerfulness());
        } while (true);

    }

}
